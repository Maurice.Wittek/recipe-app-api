resource "aws_vpc" "vpc" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-vpc" })
  )
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-gw" })
  )
}

locals {
  public_subnets = { "${data.aws_region.current.name}a" : "10.1.1.0/24", "${data.aws_region.current.name}b" : "10.1.2.0/24" }
}

resource "aws_subnet" "public_subnet" {
  for_each = local.public_subnets

  vpc_id = aws_vpc.vpc.id

  cidr_block              = each.value
  availability_zone       = each.key
  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-public-subnet-${each.key}" })
  )
}

resource "aws_route_table" "public_subnet" {
  for_each = local.public_subnets

  vpc_id = aws_vpc.vpc.id

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-route-table-public-subnet-${each.key}" })
  )
}

resource "aws_route_table_association" "public_subnet" {
  for_each = local.public_subnets

  subnet_id      = aws_subnet.public_subnet[each.key].id
  route_table_id = aws_route_table.public_subnet[each.key].id
}

resource "aws_route" "public_internet_acces" {
  for_each = local.public_subnets

  route_table_id         = aws_route_table.public_subnet[each.key].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

resource "aws_eip" "public" {
  for_each = local.public_subnets

  vpc = true

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-eip-public-subnet-${each.key}" })
  )
}

resource "aws_nat_gateway" "public" {
  for_each = local.public_subnets

  allocation_id = aws_eip.public[each.key].id
  subnet_id     = aws_subnet.public_subnet[each.key].id

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-nat-gw-public-subnet-${each.key}" })
  )
}

locals {
  private_subnets = { "${data.aws_region.current.name}a" : "10.1.10.0/24", "${data.aws_region.current.name}b" : "10.1.11.0/24" }
}

resource "aws_subnet" "private_subnet" {
  for_each = local.private_subnets

  vpc_id = aws_vpc.vpc.id

  cidr_block        = each.value
  availability_zone = each.key

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-private-subnet-${each.key}" })
  )
}

resource "aws_route_table" "private_subnet" {
  for_each = local.private_subnets

  vpc_id = aws_vpc.vpc.id

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-route-table-private-subnet-${each.key}" })
  )
}

resource "aws_route_table_association" "private_subnet" {
  for_each = local.private_subnets

  subnet_id      = aws_subnet.private_subnet[each.key].id
  route_table_id = aws_route_table.private_subnet[each.key].id
}

resource "aws_route" "private_internet_outbound" {
  for_each = local.private_subnets

  route_table_id         = aws_route_table.private_subnet[each.key].id
  nat_gateway_id         = aws_nat_gateway.public[each.key].id
  destination_cidr_block = "0.0.0.0/0"
}
