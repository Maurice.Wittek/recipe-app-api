variable "prefix" {
  default = "raa"
}

variable "project" {
  default = "recipe-app-api"
}

variable "region" {
  default = "us-west-2"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "093955452992.dkr.ecr.us-west-2.amazonaws.com/recipe-app-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "093955452992.dkr.ecr.us-west-2.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for django app"
}
