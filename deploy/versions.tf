terraform {
  required_version = ">= 1.1"

  backend "s3" {
    bucket         = "recipe-app-api-tfstate-pzhfdeceqw"
    key            = "recipe-app.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "recipe-app-api-tf-state-lock"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.1.0"
    }
  }
}
