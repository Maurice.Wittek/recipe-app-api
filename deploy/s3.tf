resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files"
  force_destroy = true
}

resource "aws_s3_bucket_acl" "public-read" {
  bucket = aws_s3_bucket.app_public_files.id
  acl    = "public-read"
}
